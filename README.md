# 21dthd4-2180608180

## Nguyễn Đức Tuấn

##

## Nguyễn Ngọc Như Bình

|Oder                   |Content                                |
| :---:                 | :---                                  |
|**Title**              | Staff login                           |
|**Value Statement**    | The staff login functionality ensures secure access and efficient management for staff members to authenticate and utilize the system's   features.|
|**Acceptance Criteria**|Staff members can enter their credentials and successfully log into the system.<br/>The system verifies the entered login information and  grants access to authorized staff members only.<br/>Incorrect login attempts are logged and appropriate security measures are in place to prevent unauthorized access.<br/>The system provides a password recovery mechanism for staff members who have forgotten their login credentials.<br/>Upon successful login, staff members are directed to their respective dashboard or landing page.|
|**Definition of Done** |Staff members can log into the system using their credentials and access their designated features and information.<br/>Appropriate security measures are implemented to protect staff login information and prevent unauthorized access.<br/>The system has a robust password recovery mechanism in place for staff members who need assistance with their login credentials.|
|**Owner**              |Binh                                   |
|**Iteration**          |This user story will be included in the next development iteration.|
|**Estimate**           |                                       |

[Staff login](FormLogin.jpg)

## Nguyễn Đức Tuấn

| Order                     | Content                                       |
| :---:                     | :---                                          |
| **Title**                 | Staff insert list of products information     |
| **Value Statement**       | As a manager, I want to insert list of products information so I can see how it functions.|
| **Acceptance Criteria**   | Acceptance Criterion 1: <br/>The system will check the validity of the data and display an error message when adding or editing product information<br/><br/>Acceptance Criterion 2:<br/>Product listings updated synchronously across all devices and sessions.<br/><br/>Acceptance Criterion 3:<br/>Stable operation, no errors or delays.|
| **Definition of Done**    | Unit Test Passed<br/>Acceptance Criteria Met<br/>Code Reviewed<br/>Functional Test Passed<br/>Non-Functional Requirements Met<br/>Product Owner Accepts User Story|
| **Owner**                 | Nguyen Duc Tuan                               |
| **Iteration**             | Unscheduled                                   |
| **Estimate**              |                                               |

![Staff insert list of products information](https://gitlab.com/ductuan.ndt/21dthd4-2180608180/-/raw/main/UI%20of%20US.jpg)

| No | Req ID  | Test Objective                                                                                       | Actual Result                                                                                                                                                                                                                                              | Expected Result                                                  |
|----|---------|------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------|
| 1  | REQ-001 | Product Database can store product information                                                       | 1. Get into Demo Product Management application.<br/> 2. Sign-in with staff account.<br/> 3. Click on My Product button.<br/> 4. Click on "Insert Product Information" button.<br/> 5. Enter complete product information.<br/> 6. Click on "Save" button. | The employee user entered the information successfully.<br/>   |
| 2  | REQ-002 | "Import Successful" is displayed when information is successfully imported into the Product Database | 1. Get into Demo Product Management application.<br/> 2. Sign-in with staff account.<br/> 3. Click on My Product button.<br/> 4. Click on "Insert Product Information" button.<br/> 5. Enter complete product information.<br/> 6. Click on "Save" button. | "Import Data Successed" message box after click on "Save" button |
| 3  | REQ-003 | "Import Failed" is displayed when information is unsuccessfully imported into the Product Database   | 1. Get into Demo Product Management application.<br/> 2. Sign-in with staff account.<br/> 3. Click on My Product button.<br/> 4. Click on "Insert Product Information" button.<br/> 5. Enter complete product information.<br/> 6. Click on "Save" button. | "Import Data Failed" message box after click on "Save" button    |

## Phạm Doãn Hoàng Dân

| Order						| Content						|
| :---:						|  :---							|
| **Title**					| Manager add product images	|
| **Value Statement**		| As a manager, I want to add images to products so I can see what the products look like |
| **Acceptance Criteria**	| _Acceptance Criterion 1_: <br/> Show product image on its avatar frame when manager successfully upload photo <br/> <br/>  _Acceptance Criterion 2_: <br/> Display a warning message when a problem occurs while uploading images |
| **Definition of Done**	| Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Acceprs User Story |
| **Owner**					| Mr. Dian						|
| **Iteration**				| Unscheduled					|
| **Estimate**				| 								|

![Image](UI.png)

## Tên họ: Trần Thanh Nguyên

## Mssv: 2180607813

## Lớp: 21DTHD4

|Title|Cashier using Product's ID to request product information|
|-----|------|
|Value statement|As a cashier,|
|               |I want to be able to use a product's ID,|
|               |So that I can quickly request its information.|
|Acceptance criteria|The system should have a database of products with their respective IDs and information.|
|                   |When the cashier enters a valid product ID, the system should retrieve and display the corresponding product information.|
|                   |The displayed information should include the product's name, price, and any other relevant details.|
|                   |If the entered product ID is invalid or not found in the database, the system should display an appropriate error message.|
|                   |The system should allow the cashier to continue processing other products after displaying the requested product's information.|
|                   |The cashier should be able to request product information multiple times during a transaction.|
|Definition done|Unit Test Passed|
|               |Acceptance Criteria Met|
|               |Code Reviewed|
|               |Functional Test Passed|
|               |Non-Functional Requirements Met|
|               |Product Owner Accepts User Story|
|Owner |Mr Nguyen|
|Iteration| |
|Estimate| |


![Alt text](Image.png)

## Find product infromation Test Case       

| No                        | Req ID                        | Test Objective                            | Test Steps                    | Expected Result           |
| :---:                     |  :---                         | :---                                      |  :---                         | :---:                     |
| **1**                     | REQ-001                       | Product Database can store product's ID,| 1. Log in to the DemoProductManager app <br/> 2. Log in with the Manager account <br/> 3. Click the My Products button <br/> 4. Enter the Product ID to search <br /> 5. Click on the search button | Have product information in datagridview |
| **2**                     | REQ-002                       | The message “Product found” displays when product information is found in the Product Database. | 1. Log in to the DemoProductManager app <br/> 2. Log in with the Manager account <br/> 3. Click the My Products button <br/> 4. Enter the Product ID to search <br /> 5. Click on the search button| The "Product Found" message box pops up within 3 seconds of clicking the "Find" button |
| **3**                     | REQ-003                       | The message “Product not found, please re-enter” displays when product information is found in the Product Database.  | 1. Log in to the DemoProductManager app <br/> 2. Log in with the Manager account <br/> 3. Click the My Products button <br/> 4. Enter the Product ID to search <br /> 5. Click on the search button | " The "Product not found, please re-enter" message box pops up within 3 seconds after clicking the "Find" button |

## Họ Tên: Lê Trung Tiến

## Mssv: 2180608093 

## Lớp: 21DTHD4

| **Title**					| manager request business report	|
|    ---                    | ---                           |
| **Value Statement**		| As a manager, I want to see my store's monthly sales history so that I can know the business situation of the store |
| **Acceptance Criteria**	| _Acceptance Criterion 1_: <br/> Given I input valid month And I input a valid year <br/> When I click the Search button <br/> And Sales statistics were calculated and displayed <br/> |
| **Definition of Done**	| Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Acceprs User Story |
| **Owner**					| Mr. Tien.P					|
| **Iteration**				| Unscheduled					|
| **Estimate**				| 								|

![QLSB](Windowsform.png)